/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import static com.promis.polymer.util.Constants.*;
import static com.promis.polymer.util.Utils.regexpReplace;

import java.util.regex.Matcher;

import com.promis.polymer.util.Constants.Regexp;
import com.promis.polymer.util.Utils.ReplaceDelegate;

public class CommentSanitizer
{
	private TypeConverter typeConverter;
	
	public void setTypeConverter(TypeConverter typeConverter)
	{
		this.typeConverter = typeConverter;
	}
	
	public String sanitizeJavaComment(String comment, final String prefix,
			final boolean acceptsNewLine)
	{
		if (comment != null)
		{
			//Fix indentation prior additional before sanitation (it depends on
			//indentation being already fixed)
			comment = prefix + Regexp.COMMENT_INDENT.matcher(comment)
					.replaceAll(prefix + " *").trim();
			comment = regexpReplace(Regexp.COMMENT_SANITIZE, comment, 
					new ReplaceDelegate()
					{
						@Override
						public String replace(Matcher matcher)
						{
							String s = matcher.group(2);
							if (s == null)
							{
								if (acceptsNewLine)
									return matcher.group();
								return "";
							}
							switch (s)
							{
							case "@class":
							case "@element":
							case "@homepage":
							case "@type":
							case "@method":
							case "@event":
							case "@extends":
								return "";
							case "@default":
								return prefix + " * <p><b>Default:</b> " + matcher.group(3) + "</p>\n"; 
							case "@property":
								return prefix + " * <p>Defined as <b>property</b></p>\n";
							case "@attribute":
								return prefix + " * <p>Defined as <b>attribute</b></p>\n";
							case "@group":
								return prefix + " * <p><b>Group:</b> " + matcher.group(3) + "</p>\n";
							case "@mixins":
								return prefix + " * <p><b>Mixins:</b> " + matcher.group(3) + "</p>\n";
							case "@status":
							{
								String arg = matcher.group(3);
								if ("unstable".equalsIgnoreCase(arg))
									return prefix + " * <p><b>Unstable!</b></p>\n";
								if ("beta".equalsIgnoreCase(arg))
									return prefix + " * <p><b>Beta!</b></p>\n";
								return matcher.group();
							}
							case "@param":
								return matcher.group();
							case "@returns":
							case "@return":
							{
								String arg = matcher.group(3);
								Matcher m = Regexp.JSDOC_ARGUMENT.matcher(arg);
								if (m.find())
								{
									String type = m.group(1);
									//Blacklisted types are kept intact
									if (! RETURN_BLACKLIST.contains(type))
									{
										type = typeConverter.getJavaType(type);
										if (! SYSTEM_TYPES.contains(type))
											type = "{@link " + type + "}";
										return prefix + " * @return " + type
												+ " " + m.group(2) + "\n";
									}
								}
								return prefix + " * @return " + arg + "\n";
							}
							
							default:
								return "";
							}
						}
					});
			comment = regexpReplace(Regexp.COMMENT_LINK, comment,
					new ReplaceDelegate()
					{
						@Override
						public String replace(Matcher matcher)
						{
							String link = matcher.group(1);
							if (link.startsWith("../"))
							{
								//Link to other polymer code
								link = link.substring(3);
								if (link.endsWith("/"))
									link = link.substring(0, link.length() - 1);
								link = typeConverter.sanitizeJavaClassName(link);
								return "{@link " + link + "}";
							}
							else if (link.startsWith("http"))
							{
								//URL
								return "{@link " + link + "}";
							}
							else if (link.startsWith("#"))
							{
								//Link to other polymer code - 2nd form
								link = link.substring(1);
								if (Regexp.ELEMENT_NAME.matcher(link).matches())
								{
									link = typeConverter.sanitizeJavaClassName(link);
									return "{@link " + link + "}";
								}
							}
							//Return entire link
							return matcher.group();
						}
					});
			comment = regexpReplace(Regexp.COMMENT_CODE_INLINE, comment,
					new ReplaceDelegate()
					{
						@Override
						public String replace(Matcher matcher)
						{
							String s = matcher.group(1).trim();
							s = s.replace("{", "");
							s = s.replace("}", "");
							return "{@code " + s + "}";
						}
					});
			//Replace comment end with something that will not terminate the doc 
			comment = Regexp.COMMENT_COMMENT.matcher(comment).replaceAll("* /");
			comment = regexpReplace(Regexp.COMMENT_HTML, comment, 
					new ReplaceDelegate()
					{
						@Override
						public String replace(Matcher matcher)
						{
							String element = matcher.group(2);
							//White-listed elements (supported by JavaDoc)
							switch (element)
							{
							case "b":
							case "i":
							case "p":
							case "code":
								return matcher.group();
							}
							return "&lt;" + matcher.group(1) + "&gt;";
						}
					});
		}
		return comment;
	}

	public String sanitizeHtmlComment(String comment, final String prefix,
			final boolean acceptsNewLine)
	{
		//Some HTML comments may already be JSDoc-like
		if (! comment.startsWith("/**"))
		{
			//Convert to JAVA comment
			comment = "/**\n" + Regexp.LINESTART.matcher(comment).replaceAll(" * ")
					+ "\n */";
		}
		//And sanitize it
		comment = sanitizeJavaComment(comment, prefix, acceptsNewLine);
		return comment;
	}

	public CommentSanitizer()
	{
		super();
	}

}