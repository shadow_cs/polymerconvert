/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import java.util.regex.Pattern;

public class Constants
{
	public static final String VOID = "void";
	public static final String STRING = "String";
	public static final String POLYMER = "Polymer";
	public static final String MIXIN = "Mixin";
	public static final String PROP_GET_PREFIX = "get ";
	public static final String PROP_SET_PREFIX = "set ";
	public static final Set<String> RETURN_BLACKLIST = new HashSet<>(
			Arrays.asList("Returns", "the"));
	public static final Set<String> SYSTEM_TYPES = new HashSet<>(
			Arrays.asList("void", "boolean", "int", "double", "number",
					"String"));
	public static final String[] HTML_PREFIX_BLACKLIST = {"index", "demo", 
		"metadata"};
	public static final String[] SCRIPT_PREFIX_BLACKLIST = {"Polymer", "Grunt", 
		"CustomElements", "HTMLImports", "ShadowDOM", "webcomponents"};
	
	public interface Regexp
	{
		//Pattern BOOLEAN = Pattern.compile("true|false");
		Pattern INT = Pattern.compile("[0-9]+");
		Pattern FLOAT = Pattern.compile("[+-]?(\\d+\\.)?\\d+([eE][+-]?\\d+)?");
		
		Pattern LINESTART = Pattern.compile("^", Pattern.MULTILINE);
		
		Pattern COMMENT_INDENT = Pattern.compile("[ \\t]+\\*");
		//Must start at start of line and end at the end to properly sanitize 
		//empty lines! 
		Pattern COMMENT_SANITIZE = Pattern.compile(
				"[ \t]* \\*[ \\t]*((@[^ ]+) ([a-zA-Z0-9_\\-\"' \\.:/{}]+))?[\n\r]+");
		Pattern COMMENT_LINK = Pattern.compile(
				"<a href=\"([^\"]+)\">([^<]+)</a>");
		Pattern COMMENT_CODE_INLINE = Pattern.compile(
				"`([a-zA-Z0-9_\\-<>/\\.\\(\\)\\{\\} :#']+?)`");
		Pattern COMMENT_COMMENT = Pattern.compile("\\*/(?!$)"); //No multiline!
		Pattern COMMENT_HTML = Pattern.compile("<([/]?([a-z]+).*?)>");
		
		Pattern JSDOC_ATTRIBUTE = Pattern.compile("\\*[ \\t]*@property (.+?)[\n\r]");
		Pattern JSDOC_ATTRIBUTE_OR_PROPERTY = Pattern.compile(
				"\\*[ \\t]*@(attribute|property) (.+?)[\n\r]");
		Pattern JSDOC_TYPE = Pattern.compile("\\*[ \\t]*@type (.+?)[\n\r]");
		Pattern JSDOC_RETURN = Pattern.compile("\\*[ \\t]*@return[s]? ([^ \n\r]+) ?(.*?)[\n\r]");
		Pattern JSDOC_METHOD = Pattern.compile("\\*[ \\t]*@method ([^ \n\r]+) ?(.*?)[\n\r]");
		Pattern JSDOC_EVENT = Pattern.compile("\\*[ \\t]*@event ([a-z0-9\\-]+)");
		/**
		 * Argument of a return or param.
		 */
		Pattern JSDOC_ARGUMENT = Pattern.compile("^([\\w{}]+)?[\\s]?(.*)$");
		
		Pattern HTMLDOC_EVENT = Pattern.compile("^@event ([a-z0-9\\-]+)", Pattern.MULTILINE);
		
		Pattern HTML_JS = Pattern.compile(".*\\.(htm[l]?|js)$", Pattern.CASE_INSENSITIVE);
		
		Pattern ELEMENT_NAME = Pattern.compile("^[a-z0-9\\-]+$");
	}

	private Constants() {}
}
