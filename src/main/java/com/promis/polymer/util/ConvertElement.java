/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import static com.promis.polymer.util.Constants.*;
import static com.promis.polymer.util.JsAstUtils.*;
import static com.promis.polymer.util.Utils.*;
import static java.lang.System.err;
import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.*;

import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;

import org.apache.commons.cli.BasicParser;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.eclipse.jdt.core.dom.AST;
import org.eclipse.wst.jsdt.core.compiler.IProblem;
import org.eclipse.wst.jsdt.core.dom.*;
import org.eclipse.wst.jsdt.core.dom.Assignment.Operator;
import org.fusesource.jansi.Ansi;
import org.fusesource.jansi.AnsiConsole;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;

import com.promis.polymer.util.Constants.Regexp;
import com.promis.polymer.util.JsAstUtils.FindInvocationVisitor;

/**
 * Converts polymer elements and their respective properties to be
 * consumable by GWT
 * @author ShadoW
 *
 */
public class ConvertElement
{
	@SuppressWarnings("static-access")
	public static void main(String[] args)
	{
		AnsiConsole.systemInstall();
		Options options = new Options();
		options.addOption(OptionBuilder
				.withDescription("Convert all HTML and JS files in given directory, "
						+ "if not specified, only given file is converted")
				.withLongOpt("dir")
				.create('d'));
		options.addOption(OptionBuilder
				.withDescription("Specify package of the output Java files")
				.hasArg()
				.withLongOpt("package")
				.create('p'));
		CommandLine cmd = null;
		try
		{
			cmd = new BasicParser().parse(options, args);
			if (cmd.getArgList().isEmpty())
				throw new ParseException("No input files specified");
		}
		catch (ParseException e)
		{
			err.println("Invalid arguments: " + e.getMessage());
			HelpFormatter formatter = new HelpFormatter();
			System.out.println("Polymer element converter");
			formatter.printHelp("ConvertElement [-d][-p package.name] \"file/dir names\"", options);
			System.exit(-1);
		}
		
		boolean dir = cmd.hasOption('d');
		
		ConvertElement convert = new ConvertElement();
		try
		{
			convert.setPkg(cmd.getOptionValue('p', "org.polymer.elements"));
			@SuppressWarnings("unchecked")
			List<String> files = cmd.getArgList();
			for (String s : files)
			{
				File file = new File(s);
				if (dir) convert.parseDir(file);
				else convert.parse(file);
			}
		}
		catch (IOException e)
		{
			err.println("  " + ansi().fg(RED).a(e.getMessage()).reset());
			return;
		}
	}

	
	private enum Mode
	{
		FILE, DIR
	}
	
	private ElementSourceWriter out = null;
	private Mode mode;
	private TypeConverter typeConverter;
	private CommentSanitizer commentSanitizer;
	private String pkg;
	private String jsSource = null; 
	
	public ConvertElement()
	{
		typeConverter = new TypeConverter();
		commentSanitizer = new CommentSanitizer();
		commentSanitizer.setTypeConverter(typeConverter);
	}
	
	public void setPkg(String pkg)
	{
		this.pkg = pkg;
	}
	
	public void parse(File file) throws IOException
	{
		out = new ElementSourceWriterJsni(System.out);
		mode = Mode.FILE;
		parseInternal(file);
	}
	
	public void parseDir(File dir) throws IOException
	{
		mode = Mode.DIR;
		
		for (File f : dir.listFiles())
		{
			if (Regexp.HTML_JS.matcher(f.getName()).matches())
			{
				parseInternal(f);
			}
		}
	}
	
	private void parseInternal(File file) throws IOException
	{
		if (file.getName().toLowerCase().endsWith(".js")) 
			parseScript(file);
		else parseHtml(file);
	}
	
	private void logFile(File file)
	{
		if (mode == Mode.DIR)
			err.println(ansi().fg(GREEN).a("File: ").reset().a(file.getName()));
	}
	
	private void parseHtml(File file) throws IOException
	{
		//Mostly dummy code, skip
		if (prefixMatches(file.getName(), HTML_PREFIX_BLACKLIST))
			return;
		logFile(file);
		byte[] encoded = Files.readAllBytes(file.toPath());
		String html = new String(encoded, "utf-8");
		//Polymer declarations contain no html/head elements, sanitize it
		html = "<html><head></head><body>" + html + "</body></html>";
		Document doc = Jsoup.parse(html);
		Elements elements;
		elements = doc.getElementsByTag("body");
		List<Node> nodes = elements.get(0).childNodes();
		List<String> comments = new ArrayList<>();
		
		for (Node node : nodes)
		{
			if (node instanceof org.jsoup.nodes.Comment)
			{
				String data = ((org.jsoup.nodes.Comment)node).getData().trim();
				//Do not include preface
				if (data.startsWith("Copyright (")) ;
				else if (data.startsWith("@license")) ;
				else comments.add(data);
			}
			else if (node instanceof Element)
			{
				Element e = (Element)node;
				if ("polymer-element".equalsIgnoreCase(e.tagName()))
				{
					parseElement(e, comments);
					comments.clear();
				}
			}
		}
	}
	
	private void parseElement(Element e, List<String> comments) throws IOException
	{
		String name = e.attr("name");
		String parent = e.attr("extends");
		String className = typeConverter.sanitizeJavaClassName(name);
		
		if (mode == Mode.DIR)
			out = new ElementSourceWriterJsni(new PrintStream(className + ".java"));
		
		try
		{
			JavaScriptUnit jsUnit = null;
			Elements elements = e.getElementsByTag("script");
			if (elements.size() > 0)
			{
				String js = elements.get(0).html();
				jsUnit = readScript(js);
			}
			
			writeClassIntro(className, parent, comments);			
			writePreface(name, className, comments, jsUnit);
			
			if (jsUnit != null)
				parsePolymerScript(jsUnit, name);
			
			out.endClass();
		}
		finally
		{
			if (mode == Mode.DIR)
			{
				out.close();
				out = null;
			}
		}
	}
	
	private void parseScript(File file) throws IOException
	{
		//Polymer support libraries, skip
		if (prefixMatches(file.getName(), SCRIPT_PREFIX_BLACKLIST))
			return;
		logFile(file);
		byte[] encoded = Files.readAllBytes(file.toPath());
		String js = new String(encoded, "utf-8");
		JavaScriptUnit jsUnit = readScript(js);
		List<?> statements = jsUnit.statements();
		if (statements.size() <= 0)
			return;
		for (Object statement : statements)
		{
			if (statement instanceof ExpressionStatement)
			{
				Expression expression = ((ExpressionStatement)statement)
						.getExpression();
				if (! (expression instanceof Assignment))
					continue;

				Assignment assignment = (Assignment)expression;
				
				if (assignment.getOperator() != Operator.ASSIGN)
					continue;
				if (! (assignment.getLeftHandSide() instanceof FieldAccess))
					continue;
				if (! (assignment.getRightHandSide() instanceof ObjectLiteral))
					continue;
				FieldAccess field = (FieldAccess)assignment.getLeftHandSide();
				if (! POLYMER.equals(getAstString(field.getExpression())))
					continue;
				
				ObjectLiteral object = (ObjectLiteral)assignment.getRightHandSide();
				ObjectLiteralField publish = findAstField(object, "mixinPublish");
				if (publish != null 
						&& publish.getInitializer() instanceof ObjectLiteral)
				{
					String className = getAstString(field.getName()) + MIXIN;
					String comment = findAstDocComment(jsUnit,
							expression.getParent());
					if (comment == null)
					{
						//Try to match first comment (not sure why the default 
						//mapper is unable to find it)
						@SuppressWarnings("unchecked")
						List<Comment> jsComments = jsUnit.getCommentList();
						if (jsComments.size() > 0)
						{
							Comment jsComment = jsComments.get(0);
							if (jsComment.isDocComment())
							{
								int pos = jsComment.getStartPosition()
										+ jsComment.getLength();
								int stmPos = expression.getParent()
										.getStartPosition();
								//Come reasonable limit
								if (stmPos - pos < 6)
									comment = jsComment.toString();
							}
						}
					}
					List<String> comments = new ArrayList<>();
					if (comment != null)
						comments.add(comment);
					parseMixin(className, (ObjectLiteral)publish.getInitializer(),
							comments, jsUnit);
				}
			}
		}
	}
	
	private void parseMixin(String className, ObjectLiteral publish,
			List<String> comments, JavaScriptUnit jsUnit) throws IOException
	{
		if (mode == Mode.DIR)
			out = new ElementSourceWriterJsni(new PrintStream(className + ".java"));
		
		try
		{
			writeClassIntro(className, null, comments);			
			writePreface(null, className, comments, jsUnit);
			parsePublish(publish, jsUnit);
			
			out.endClass();
		}
		finally
		{
			if (mode == Mode.DIR)
			{
				out.close();
				out = null;
			}
		}
	}
	
	private JavaScriptUnit readScript(String js)
	{
		JavaScriptUnit jsUnit;
		boolean /*parsedOk, */continueParse;
		do
		{
			@SuppressWarnings("deprecation")
			ASTParser parser = ASTParser.newParser(AST.JLS3);
			parser.setSource(js.toCharArray());
			jsUnit = (JavaScriptUnit)parser.createAST(null);
			//parsedOk = true;
			continueParse = false;
			for (IProblem problem : jsUnit.getProblems())
			{
				//parsedOk = false;
				boolean autoCorrected = false;
				String[] args = problem.getArguments();
				//All auto-corrections must not change length of js
				if (args.length == 2 && "for".equals(args[0])
						&& "Identifier".equals(args[1]))
				{
					//Fixes core-label parse error, for is a reserved
					//word for our parser so replace it with _or
					js = replaceCharAt(js, problem.getSourceStart(), '_');
					autoCorrected = true;
				}
				continueParse |= autoCorrected;
				
				Ansi s = ansi().fg(RED).a("JS: ")
					.a(problem.getSourceLineNumber()).reset()
					.a(": ").a(problem.getMessage());
				if (autoCorrected)
					s.fg(CYAN).a(" - autocorrected").reset();
				err.println(s);
			}
		} while (continueParse);
		jsSource = js;
		return jsUnit;
	}
	
	private void writeClassIntro(String className, String parent,
			List<String> comments)
	{
		if (mode == Mode.DIR)
			err.println(ansi().fg(MAGENTA).a("Class: ").reset().a(className));
		out.beginUnit(pkg);
		
		if ((parent != null) && (! parent.isEmpty()))
		{
			parent = typeConverter.sanitizeJavaClassName(parent);
		}
		else parent =  "HtmlElement";
		if (comments.size() > 0)
		{
			String comment = comments.get(0);
			comment = commentSanitizer.sanitizeHtmlComment(comment, "", true);
			out.emitComment(comment);
		}
		out.beginClass(className, parent);
	}
	
	private void writePreface(String tagName, String className,
			List<String> comments, JavaScriptUnit jsUnit)
	{
		if (tagName != null)
			out.emitConstant(STRING, "TAG", "\"" + tagName + "\"");
		
		if (comments.size() >= 2)
		{
			//Inspect events
			for (int i = 1, c = comments.size(); i < c; i++)
			{
				String comment = comments.get(i);
				Matcher matcher = Regexp.HTMLDOC_EVENT.matcher(comment);
				if (matcher.find())
				{
					//TODO nesting event types (create JSNI wrapper for objects inside events)
					String event = matcher.group(1);
					comment = commentSanitizer.sanitizeHtmlComment(
							comment, "\t", false);
					writeEvent(event, comment);
				}
			}
		}
		if (jsUnit != null)
		{
			//Inspect JS events
			@SuppressWarnings("unchecked")
			List<Comment> jsComments =  jsUnit.getCommentList();
			for (Comment comment : jsComments)
			{
				if (comment.isDocComment())
				{
					String sComment = comment.toString();
					Matcher matcher = Regexp.JSDOC_EVENT.matcher(sComment);
					if (matcher.find())
					{
						String event = matcher.group(1);
						sComment = commentSanitizer.sanitizeJavaComment(
								sComment, "\t", false);
						writeEvent(event, sComment);
					}
				}
			}
		}

		if (tagName != null)
			out.emitPreface(className);
		else out.emitCtor(className);
	}
	
	private void writeEvent(String event, String comment)
	{
		String field = "EVENT_" + event.toUpperCase().replace('-', '_');
		out.emitComment(comment);
		out.emitConstant(STRING, field, "\"" + event + "\"");
	}
	
	private void parsePolymerScript(JavaScriptUnit jsUnit, String name)
	{
		FunctionInvocation polymer = null;
		try
		{
			jsUnit.accept(new FindInvocationVisitor(POLYMER));
		}
		catch (FindInvocationVisitor.Found f)
		{
			polymer = f.getNode();
		}
		if (polymer == null)
			return;
		List<?> arguments = polymer.arguments();
		int paramIndex;
		switch (arguments.size())
		{
		case 2:
			if (! name.equals(getAstString(arguments.get(0))))
			{
				printErr("Polymer declaration does not match element declaration " + name);
				return;
			}
			//$FALL-THROUGH$
		case 1:
			paramIndex = arguments.size() - 1;
			break;
		default:
			printErr("Invalid Polymer declaration argument count");
			return;
		}
		Object param = arguments.get(paramIndex);
		if (param instanceof FunctionInvocation)
		{
			FunctionInvocation invocation = (FunctionInvocation)param;
			if (POLYMER.equals(getAstString(invocation.getExpression())) 
					&& getAstString(invocation.getName()).startsWith("mixin"))
			{
				//Mixin invocation
				param = invocation.arguments().get(0); 
			}
			else
			{
				printErr("Invalid Polymer declaration arguments, "
						+ "nested function call or unknown mixin");
				return;
			}				
		}
		if (param instanceof SimpleName)
		{
			//Argument is a variable
			String varName = getAstString(param); 
			param = null;
			//First parent is the invocation second should be the block
			ASTNode parent = polymer.getParent().getParent();
			if (! (parent instanceof Block))
			{
				printErr("Invalid Polymer declaration, cannot find outer block");
				return;
			}
			Block block = (Block)parent;
			@SuppressWarnings("unchecked")
			List<Statement> statements = block.statements();
			outer:
			for (Statement statement : statements)
			{
				if (statement instanceof VariableDeclarationStatement)
				{
					//var param = {...};
					VariableDeclarationStatement var = 
							(VariableDeclarationStatement)statement;
					@SuppressWarnings("unchecked")
					List<VariableDeclarationFragment> fragments = var.fragments();
					for (VariableDeclarationFragment fragment : fragments)
					{
						if (varName.equals(getAstString(fragment.getName())))
						{
							param = fragment.getInitializer();
							break outer;
						}
					}
					var = null;
				}
				if (statement instanceof ExpressionStatement)
				{
					//param = {...};
					ExpressionStatement expStm = (ExpressionStatement)statement; 
					if (expStm.getExpression() instanceof Assignment)
					{
						Assignment assignment = (Assignment)expStm.getExpression();
						if (assignment.getOperator() == Operator.ASSIGN
								&& varName.equals(getAstString(
										assignment.getLeftHandSide())))
						{
							param = assignment.getRightHandSide();
							break outer;
						}
					}
				}
			}
			if (param == null)
			{
				printErr("Invalid Polymer argument not found: " + param);
				return;
			}
		}
		if (param instanceof StringLiteral)
		{
			//No arguments just component name
			if (! name.equals(getAstString(arguments.get(0))))
				printErr("Polymer declaration does not match element declaration " + name);
			return;
		}
		if (! (param instanceof ObjectLiteral))
		{
			printErr("Invalid Polymer declaration arguments");
			return;
		}
		ObjectLiteral paramObject = (ObjectLiteral)param;
		
		//Parse published fields
		ObjectLiteralField publish = findAstField(paramObject,
				"publish");
		if (publish != null)
		{
			Expression initializer = publish.getInitializer();
			if (initializer instanceof ObjectLiteral)
				parsePublish((ObjectLiteral)initializer, jsUnit);
		}
		
		//Parse mixins
		try
		{
			//Only mixin2 (ie. CoreFocusable is of interest to us)
			//CoreResizer and CoreResizeable `mixin` do not publish anything			
			jsUnit.accept(new FindInvocationVisitor("mixin2"));
		}
		catch (FindInvocationVisitor.Found f)
		{
			FunctionInvocation mixin = f.getNode();
			arguments = mixin.arguments();
			//Let it fail if the declaration is invalid
			FieldAccess field = (FieldAccess)arguments.get(1);
			field.getClass();
			name = getAstString(field.getName());
			String comment = "Returns `" + name + "` mixin wrapper";
			comment = commentSanitizer.sanitizeHtmlComment(comment, "\t", false);
			writeMethod(name + MIXIN, "as" + name, comment);
		}
		
		//Parse public methods, attributes and properties 
		@SuppressWarnings("unchecked")
		List<ObjectLiteralField> fields = paramObject.fields();
		for (ObjectLiteralField field : fields)
		{
			Expression initializer = field.getInitializer();
			if (initializer instanceof FunctionExpression)
				parseMethod(getAstString(field.getFieldName()),
						(FunctionExpression)initializer, jsUnit);
			else if ("MISSING".equals(getAstString(initializer)))
			{
				//Probably property getter or setter JSDT is unable to parse
				String data = jsSource.substring(field.getStartPosition(),
						field.getStartPosition() + field.getLength()).trim();
				String comment = findAstDocComment(jsUnit, field);
				if ((comment != null) && (data.startsWith(PROP_GET_PREFIX) ||
						data.startsWith(PROP_SET_PREFIX)))
				{
					//Element is most likely return type
					String type = "element";
					String fieldName;
					Matcher m = Regexp.JSDOC_ATTRIBUTE.matcher(comment);
					if (m.find())
						fieldName = m.group(1);
					else
					{
						int i = data.indexOf('(');
						//Some reasonable limit for name length 
						if ((i > 32) || (i < 0))
							continue;
						fieldName = data.substring(4, i);
					}
					type = typeConverter.sanitizeType(type, comment);
					comment = commentSanitizer.sanitizeJavaComment(
							comment, "\t", false);
					if (data.startsWith(PROP_GET_PREFIX))
						out.emitPropertyGetter(type, fieldName, comment);
					else if (data.startsWith(PROP_SET_PREFIX))
						out.emitPropertySetter(type, fieldName, comment);
				}
			}
			else parseProperty(field, jsUnit, true);
		}
	}

	private void parsePublish(ObjectLiteral params, JavaScriptUnit jsUnit)
	{
		@SuppressWarnings("unchecked")
		List<ObjectLiteralField> fields = params.fields();
		for (ObjectLiteralField field : fields)
		{
			parseProperty(field, jsUnit, false);
		}
	}
	
	private void parseProperty(ObjectLiteralField field, JavaScriptUnit jsUnit,
			boolean needsComment)
	{
		String comment = findAstDocComment(jsUnit, field);
		if (needsComment)
		{
			if (comment == null)
				return;
			//Attribute or property needed in order to mark the field as 
			//published
			if (! Regexp.JSDOC_ATTRIBUTE_OR_PROPERTY.matcher(comment).find())
				return;
		}
		String name = getAstString(field.getFieldName());
		Expression type = field.getInitializer();
		if (type instanceof ObjectLiteral)
		{
			//Property definition
			ObjectLiteralField value = findAstField((ObjectLiteral)type,
					"value");
			type = value != null ? value.getInitializer() : null;
		}
		//else Default value
		
		writeProperty(name, comment, typeConverter.guessType(type));
	}
	
	private void writeProperty(String name, String comment, String type)
	{
		type = typeConverter.sanitizeType(type, comment);
		writePropMethods(type, name, comment);
	}
	
	private void writePropMethods(String type, String name, String comment)
	{
		comment = commentSanitizer.sanitizeJavaComment(comment, "\t", false);
		
		out.emitPropertyGetter(type, name, comment);
		out.emitPropertySetter(type, name, comment);
	}

	private void parseMethod(String name, FunctionExpression method,
			JavaScriptUnit jsUnit)
	{
		String comment = findAstDocComment(jsUnit, method.getParent());
		if (comment == null)
			return;
		Matcher methodMatcher = Regexp.JSDOC_METHOD.matcher(comment);
		if (! methodMatcher.find())
		{
			printErr("Method not found in JSDoc: " + name);
			return;
		}
		String docName = methodMatcher.group(1);
		if (! name.equals(docName))
		{
			printErr("Method name do not match in JSDoc: " + name);
			return;
		}
		Matcher returnMatcher = Regexp.JSDOC_RETURN.matcher(comment);
		String type = returnMatcher.find() ? returnMatcher.group(1) : null; 
		type = typeConverter.getJavaType(type);
		writeMethod(type, name, comment);
	}
	
	private void writeMethod(String type, String name, String comment)
	{
		//TODO args
		if (comment != null)
			comment = commentSanitizer.sanitizeJavaComment(comment, "\t", false);
		out.emitMethodInvocation(type, name, comment);
	}
}
