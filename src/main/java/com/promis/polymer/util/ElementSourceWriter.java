/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import java.io.Closeable;
import java.io.PrintStream;

public abstract class ElementSourceWriter implements Closeable
{
	protected static final String VOID = "void";

	protected static String sanitizeJavaPropMethodSuffix(String identifier)
	{
		assert identifier.length() >= 1;
		return Character.toUpperCase(identifier.charAt(0))
				+ identifier.substring(1);
	}
	
	private final PrintStream out;

	public ElementSourceWriter(PrintStream out)
	{
		this.out = out;
	}

	protected final void println()
	{
		out.println();
	}
	
	protected final void println(String s)
	{
		out.println(s);
	}
	
	protected final void println(String fmt, Object... args)
	{
		out.println(String.format(fmt, args));
	}
	
	public abstract void beginUnit(String pkg);

	public abstract void beginClass(String className, String parent);

	public abstract void emitComment(String comment);

	public abstract void emitConstant(String type, String name, String value);
	
	public abstract void emitCtor(String className);

	public abstract void emitPreface(String className);

	public abstract void emitPropertyGetter(String type, String name, String comment);

	public abstract void emitPropertySetter(String type, String name, String comment);

	public abstract void emitMethodInvocation(String type, String name, String comment);

	public abstract void endClass();

	@Override
	public final void close()
	{
		out.close();
	}
}