/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import java.io.PrintStream;

public class ElementSourceWriterJsni extends ElementSourceWriter
{
	public ElementSourceWriterJsni(PrintStream out)
	{
		super(out);
	}

	private void writeMethodHeader(String type, String name, String args, String comment)
	{
		println();
		if (comment != null)
			println(comment);
		println("\tpublic final native " + type + " " + name + "(" + args + ")");
		println("\t/*-{");
	}	
	
	private void writeMethodFooter()
	{
		println("\t}-*/;");
	}
	
	@Override
	public void beginUnit(String pkg)
	{
		println("package " + pkg + ";\n");
		println("import com.google.gwt.core.client.JavaScriptObject;");
		println("import com.google.gwt.dom.client.Document;");
		println("import com.google.gwt.dom.client.Element;");
		println("import com.google.gwt.dom.client.Node;\n");
	}
	
	@Override
	public void beginClass(String className, String parent)
	{
		println("public class " + className + " extends " + parent);
		println("{");
	}
	
	@Override
	public void emitComment(String comment)
	{
		println(comment);
	}
	
	@Override
	public void emitConstant(String type, String name, String value)
	{
		println("\tpublic static final %s %s = %s;\n", type, name, value);
	}
	
	@Override
	public void emitCtor(String className)
	{
		println("\tprotected " + className + "() {}\n");
	}
	
	@Override
	public void emitPreface(String className)
	{
		emitCtor(className); 
		println("\t/**");
		println("\t * Assert that the given {@link Element} is compatible with this class and");
		println("\t * automatically typecast it.");
		println("\t */");
		println("\tpublic static " + className + " as(Element elem)");
		println("\t{");
		println("\t\tassert is(elem);");
		println("\t\treturn (" + className + ")elem;");
		println("\t}");
		println("\t");
		println("\t/**");
		println("\t * Determines whether the given {@link JavaScriptObject} can be cast to this");
		println("\t * class. A <code>null</code> object will cause this method to return");
		println("\t * <code>false</code>.");
		println("\t */");
		println("\tpublic static boolean is(JavaScriptObject o)");
		println("\t{");
		println("\t\tif (Element.is(o))");
		println("\t\t{");
		println("\t\t\treturn is((Element)o);");
		println("\t\t}");
		println("\t\treturn false;");
		println("\t}");
		println("\t");
		println("\t/**");
		println("\t * Determine whether the given {@link Node} can be cast to this class. A");
		println("\t * <code>null</code> node will cause this method to return");
		println("\t * <code>false</code>.");
		println("\t */");
		println("\tpublic static boolean is(Node node)");
		println("\t{");
		println("\t\tif (Element.is(node))");
		println("\t\t{");
		println("\t\t\treturn is((Element)node);");
		println("\t\t}");
		println("\t\treturn false;");
		println("\t}");
		println("\t");
		println("\t/**");
		println("\t * Determine whether the given {@link Element} can be cast to this class. A");
		println("\t * <code>null</code> node will cause this method to return");
		println("\t * <code>false</code>.");
		println("\t */");
		println("\tpublic static boolean is(Element elem)");
		println("\t{");
		println("\t\treturn elem != null && elem.hasTagName(TAG);");
		println("\t}");
		println("\t");
		println("\t/**");
		println("\t * Creates a new element.");
		println("\t */");
		println("\tpublic static " + className + " create(Document document)");
		println("\t{");
		println("\t\treturn as(document.createElement(TAG));");
		println("\t}");
		println("\t");
		println("\t/**");
		println("\t * Creates a new element.");
		println("\t */");
		println("\tpublic static " + className + " create()");
		println("\t{");
		println("\t\treturn create(Document.get());");
		println("\t}");
	}
	
	@Override
	public void emitPropertyGetter(String type, String name, String comment)
	{
		String fName = sanitizeJavaPropMethodSuffix(name);
		writeMethodHeader(type, "get" + fName, "", comment);
		println("\t\treturn this." + name + ";");
		writeMethodFooter();
	}
	
	@Override
	public void emitPropertySetter(String type, String name, String comment)
	{
		String fName = sanitizeJavaPropMethodSuffix(name);
		writeMethodHeader(VOID, "set" + fName, type + " " + name, comment);
		println("\t\tthis." + name + " = " + name + ";");
		writeMethodFooter();
	}
	
	@Override
	public void emitMethodInvocation(String type, String name, String comment)
	{
		writeMethodHeader(type, name, "", comment);
		StringBuilder line = new StringBuilder("\t\t");
		if (! VOID.equals(type))
			line.append("return ");
		line.append("this.").append(name).append("();");
		println(line.toString());
		writeMethodFooter();
	}
	
	@Override
	public void endClass()
	{
		println("}");
	}
}
