/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import static com.promis.polymer.util.Constants.*;

import java.util.regex.Matcher;

import org.eclipse.wst.jsdt.core.dom.BooleanLiteral;
import org.eclipse.wst.jsdt.core.dom.Expression;
import org.eclipse.wst.jsdt.core.dom.NumberLiteral;
import org.eclipse.wst.jsdt.core.dom.StringLiteral;

import com.promis.polymer.util.Constants.Regexp;

public class TypeConverter
{
	public TypeConverter()
	{
		super();
	}

	public String getJavaType(String guessedType)
	{
		if (guessedType == null)
			return VOID;
		//Sometimes types are surrounded in parenthesis
		if (guessedType.startsWith("{") && guessedType.endsWith("}"))
			guessedType = guessedType.substring(1, guessedType.length() - 1);
		switch (guessedType)
		{
		case "integer":
			return "int";
		case "string":
			return STRING;
		case "node":
			return "Node";
		case "element":
			return "Element";
		//Not all numbers are doubles it is better to correct these by hand
		/*case "number":
			type = "Double";
			break;*/
		case "Object":
		case "object":
			return "JavaScriptObject";
		case "Array":
		case "array":
			return "JsArray<?>";
		default:
			//Blacklisted return types are considered object since we cannot
			//know better.
			if (RETURN_BLACKLIST.contains(guessedType))
				return "JavaScriptObject";
			//Enums or mixed type values
			//TODO may be mixed with objects since JS needs no type safety
			if (guessedType.contains("|"))
				return "String";
			return guessedType;
		}
	}

	public String guessType(Expression type)
	{
		if (type instanceof StringLiteral)
			return "string";
		if (type instanceof BooleanLiteral)
			return "boolean";
		if (type instanceof NumberLiteral)
		{
			String value = ((NumberLiteral)type).getToken();
			if (Regexp.INT.matcher(value).matches())
				return "int";
			if (Regexp.FLOAT.matcher(value).matches())
				return "double";
			return "number";
		}
		return null;
	}
	
	public String sanitizeType(String type, String comment)
	{
		if (type == null) type = "null"; //Produces an error so it is spotted more easily in the IDE
		if (comment != null)
		{
			Matcher typeMatcher = Regexp.JSDOC_TYPE.matcher(comment);
			if (typeMatcher.find())
			{
				type = typeMatcher.group(1);				
			}
		}
		
		return getJavaType(type);
	}
	
	public String sanitizeJavaClassName(String name)
	{
		StringBuilder sb =  new StringBuilder();
		sb.append(Character.toUpperCase(name.charAt(0)));
		for (int i = 1; i < name.length(); i++)
		{
			char c = name.charAt(i);
			if (c == '-')
			{
				i++;
				if (i >= name.length())
					break;
				c = Character.toUpperCase(name.charAt(i));
			}
			sb.append(c);
		}
		sb.append("Element");
		return sb.toString();
	}
}