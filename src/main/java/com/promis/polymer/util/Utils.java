/*
 * This Source Code Form is subject to the terms of the Mozilla Public License, 
 * v. 2.0. If a copy of the MPL was not distributed with this file,
 * You can obtain one at http://mozilla.org/MPL/2.0/.
 */

package com.promis.polymer.util;

import static java.lang.System.err;
import static org.fusesource.jansi.Ansi.ansi;
import static org.fusesource.jansi.Ansi.Color.RED;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Utils
{
	private Utils() {}
	
	public interface ReplaceDelegate
	{
		String replace(Matcher matcher);
	}
	
	public static String replaceCharAt(String s, int at, char c)
	{
		return s.substring(0, at) + c + s.substring(at + 1);
	}
	
	public static final String regexpReplace(Pattern pattern, String text,
			ReplaceDelegate delegate)
	{
		StringBuffer result = new StringBuffer();
		Matcher matcher = pattern.matcher(text);
		while (matcher.find())
		{
			String replacement = delegate.replace(matcher);
			//Dollar sign needs to be escaped (Pattern.quote does not work here)
			replacement = replacement.replace("$", "\\$");
			matcher.appendReplacement(result, replacement);
		} 
		matcher.appendTail(result);
		return result.toString();
	}
	
	public static final boolean prefixMatches(String name, String[] prefixes)
	{
		name = name.toLowerCase();
		for (String s : prefixes)
		{
			if (name.startsWith(s.toLowerCase()))
				return true;
		}
		return false;
	}
	
	public static void printErr(String s)
	{
		err.println(ansi().fg(RED).a(s).reset());
	}

}
